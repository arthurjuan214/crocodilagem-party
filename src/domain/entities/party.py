import datetime
from dataclasses import dataclass
from domain.entities.entity import Entity
from domain.entities.user import User
from domain.entities.address import Address

from domain.enums.party_type import PartyType

@dataclass
class Party(Entity):
    name: str
    description: str
    creator: User
    start_at: datetime
    end_at: datetime
    party_type = PartyType
    guests: list() #User list
    address: Address
