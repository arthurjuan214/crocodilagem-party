from dataclasses import dataclass, field
import datetime
from typing import Optional, Union
import uuid

@dataclass(kw_only=True)
class Entity:
    id: str =  field(default_factory=lambda: str(uuid.uuid4()))
    created_at: Union[datetime.datetime, None] = datetime.datetime.now()
    updated_at:  Union[datetime.datetime, None] = datetime.datetime.now()
