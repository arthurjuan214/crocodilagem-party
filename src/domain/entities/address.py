from dataclasses import dataclass

from domain.entities.entity import Entity
@dataclass
class Address(Entity):
    street: str
    number: int
    neighborhood: str
    city: str
    state: str
    country: str
