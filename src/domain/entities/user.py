from dataclasses import dataclass
from domain.entities.entity import Entity
from typing import Optional

@dataclass(kw_only=True)
class User(Entity):
    first_name: str
    last_name: str
    email: str
    password: str
    party: Optional[list]#party list 
    checkin_party: Optional[list] #party list
