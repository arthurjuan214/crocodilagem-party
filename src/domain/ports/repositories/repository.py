from abc import ABC, abstractmethod

class IRepository(ABC):
    
    @abstractmethod
    def add(model):
        pass

    @abstractmethod
    def remove(model):
        pass
