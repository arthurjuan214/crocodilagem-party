from abc import abstractmethod
from domain.ports.repositories.repository import IRepository
from domain.entities.user import User
class IUserRepository(IRepository):

    @abstractmethod
    def get_user_by_name(name: str) -> User:
        pass

    @abstractmethod
    def get_user_by_email(email: str) -> User:
        pass