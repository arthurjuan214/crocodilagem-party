from abc import ABC, abstractmethod

class ITokenService(ABC):
    @abstractmethod
    def generate_token(userDto) -> str:
        pass
