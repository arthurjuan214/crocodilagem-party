from sqlalchemy import create_engine
from infra.data.models.party_model import PartyModel
from infra.data.models.user_model import UserModel
from infra.data.models.users_parties_model import UsersPartiesModel
from infra.data.models.model import Model
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

class DatabaseHandler:
    def __init__(self) -> None:
        self._db_engine = "sqlite:///storage.db"
        self.session = None    

    def get_engine(self):
        engine = create_engine(self._db_engine, echo=True)
        return engine

    def init_db(self) -> bool:
        engine = self.get_engine()
        Model.metadata.create_all(engine) 
        return True
    
    def __enter__(self):
        engine = create_engine(self._db_engine)
        session_maker = sessionmaker()
        self.session = session_maker(bind=engine)
        return self

    def get_db(self):
        session_maker = sessionmaker()
        Session = session_maker(bind=self.get_engine())
        return Session
