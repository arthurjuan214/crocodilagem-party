from sqlalchemy import Column, Integer, String, DateTime, func
from sqlalchemy.ext.declarative import declarative_base
import uuid
Base=declarative_base()

class Model(Base):
    __abstract__ = True
    # id = Column(String(36), primary_key=True, default=str(uuid.uuid4()))
    id = Column(String(36), primary_key=True)
    created_at = Column(DateTime, default=func.now())
    updated_at = Column(DateTime, default=func.now(), onupdate=func.now())

