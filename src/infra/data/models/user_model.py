from sqlalchemy import Integer, Column, String
from infra.data.models.model import Model
from sqlalchemy.orm import relationship

from infra.data.models.party_model import PartyModel
from infra.data.models.users_parties_model import UsersPartiesModel 
from domain.entities.user import User




class UserModel(Model):   
    __tablename__ = "users"

    first_name = Column(String(50), nullable=False)
    last_name = Column(String(50), nullable=False)
    email = Column(String(255), nullable=False, unique=True)
    password = Column(String(255), nullable=False)
    created_parties = relationship("PartyModel", backref="created_by")
    checkin_parties = relationship("PartyModel", secondary=UsersPartiesModel.party_user_association, backref="checked_in_users")

    def __repr__(self):
        return f"User: [name={self.first_name}]"

    def to_entity(self) -> User:
        
        return User(id=self.id, first_name=self.first_name,
                    last_name=self.last_name, email=self.email,
                    password=self.password, created_at=self.created_at,
                    updated_at=self.updated_at, checkin_party=self.checkin_party,
                    party=self.created_parties)
    