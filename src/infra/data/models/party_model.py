from infra.data.models.model import Model


from sqlalchemy import Integer, Column, String, ForeignKey, DateTime, Enum
from sqlalchemy.orm import relationship
from infra.data.models.users_parties_model import UsersPartiesModel

from domain.enums.party_type import PartyType

class PartyModel(Model):
    __tablename__ = "parties"

    name = Column(String(50), nullable=False)
    description = Column(String(500))
    creator = Column(Integer, ForeignKey('users.id'))
    guests = relationship("UserModel", secondary=UsersPartiesModel.party_user_association, backref="participated_parties")
    start_at = Column(DateTime)
    end_at=Column(DateTime)
    party_type = Column(Enum(PartyType))
