from infra.data.models.model import Model
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import Table, Integer, ForeignKey, Column, String

Base = declarative_base()

class UsersPartiesModel():
    party_user_association = Table(
    'party_user_association',
    Model.metadata,
    Column('party_id', String(36), ForeignKey('parties.id')),
    Column('user_id', String(36), ForeignKey('users.id'))
    )
    
    
