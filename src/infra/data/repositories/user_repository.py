from domain.entities.user import User
from domain.ports.repositories.user_repository import IUserRepository
from infra.data.database import DatabaseHandler
from infra.data.models.user_model import UserModel
class UserRepository(IUserRepository):
    
    def __init__(self) -> None:
        self.db = DatabaseHandler()
        self.__db = self.db.get_db()

    def get_user_by_email(self, email: str) -> User:
   
        result = self.__db.query(UserModel).filter(UserModel.email == email).first()
        print(result)
        if result is None:
            return None
        print(f"OMG THE RESULT: %s" % result)

        entity = User(id=result.id,first_name=result.first_name,last_name=result.last_name,
            email=result.email, password=result.password,
                      party=None, checkin_party=None, created_at=result.created_at, updated_at=result.updated_at)

        return entity


        
    def add(self, model):
       
        new_model =  self.__db.add(model)
        self.__db.commit()
        return model

    def remove(model):
        pass

    def get_user_by_name(name: str):
        pass
