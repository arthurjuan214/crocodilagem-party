from domain.ports.token_service import ITokenService
import jwt

class TokenService(ITokenService):

    def generate_token(self, user) -> str:
        key="secret@123"
        payload={
            "sub":user.id,
            "email":user.email,
            "first_name": user.first_name,
            "last_name": user.last_name
        }

        token = jwt.encode(payload=payload, key=key, algorithm="HS256")
        return token