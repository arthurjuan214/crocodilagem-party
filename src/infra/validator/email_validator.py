import re

class EmailValidator():

    def is_valid(self, email: str)-> bool:
        match = re.match(r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,7}\b',email)
        return bool(match)
