from flask import Blueprint, render_template


HomeController = Blueprint("home", __name__, url_prefix="")

@HomeController.get("/")
@HomeController.get("/home")
def home():
        return render_template("pages/home.html")
