from flask import Blueprint, request, make_response

from core.use_cases.create_party import CreateParty
from core.models.request.party_create_request import PartyCreateRequest
PartyController = Blueprint("party",__name__, url_prefix="/api/party")

class Party:
    def __init__(self, createParty: CreateParty):
        self._createParty = createParty

    @PartyController.get("/list")
    def list_party():
        return "party uhul"

    @PartyController.post("/create")
    def create_party(self):

        dto = PartyCreateRequest(request.form)
        response = self._createParty.create(dto)
        return response
        

        


