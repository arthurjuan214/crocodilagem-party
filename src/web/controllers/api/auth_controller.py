from flask import Blueprint, request, make_response
from injector import inject
from core.models.request.user_register import UserRegister
from core.use_cases.add_user import AddUser
import json

AuthController = Blueprint("auth", __name__, url_prefix="/api/auth")


@AuthController.route("/register", methods=["POST"])
@inject
def register():
        try:
                _addUser = AddUser()
                data = request.json
                userDto: UserRegister = UserRegister(
                        first_name=data.get("first_name"),
                        last_name=data.get("last_name"),
                        email=data.get("email"),
                        password=data.get("password"),
                )

                result = _addUser.add(userDto)
                # response = make_response({"response": result})
                return {"response": result}
        except Exception as e:  
                response =  make_response({
                        "success": False,
                        "response":"error",
                        "error": e.args})
                return response


