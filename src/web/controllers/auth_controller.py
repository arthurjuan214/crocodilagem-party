from flask import Blueprint, render_template

AuthPagesController = Blueprint("authPages", __name__, url_prefix="")


@AuthPagesController.get("/register")
def register():
    return render_template("pages/register.html")
