from flask import Flask
from flask_injector import FlaskInjector
from injector import Binder
from web.config.config import Configuration
from web.config.dependencies import configure

app = Flask(__name__, template_folder='views', static_folder="views/assets")


conf = Configuration(app)
conf.configureBlueprint()
conf.configureDatabase()

if __name__ == "__main__":
    app.run()
    FlaskInjector(app=app, modules=[configure])