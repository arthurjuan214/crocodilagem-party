from flask import Flask
from injector import inject, Binder, singleton
from flask_injector import FlaskInjector

from web.controllers.home_controller import HomeController
from web.controllers.api.party_controller import PartyController
from web.controllers.api.auth_controller import AuthController
from web.controllers.auth_controller import AuthPagesController


from infra.data.database import DatabaseHandler


class Configuration:

    def __init__(self, app):
        self._app = app


    def configureBlueprint(self) -> None:
        self._app.register_blueprint(HomeController)
        self._app.register_blueprint(PartyController)
        self._app.register_blueprint(AuthController)
        self._app.register_blueprint(AuthPagesController)

    def configureDatabase(self) -> None:
        db_handler = DatabaseHandler()
        db_handler.init_db()

        

   
    # def Configure(self):
   #     self.configureBlueprint()
   #     self.configureDatabase()
