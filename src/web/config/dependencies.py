from injector import singleton
from domain.ports.repositories.user_repository import IUserRepository
from infra.data.repositories.user_repository import UserRepository
from core.use_cases.add_user import AddUser

def configure(binder) -> None:
    binder.bind(IUserRepository, to=UserRepository, scope=singleton)
    binder.bind(AddUser, to=AddUser, scope=singleton)