from dataclasses import dataclass
from domain.entities.user import User

@dataclass
class UserRegister:
    first_name: str
    last_name: str
    email: str
    password: str

    def to_entity(self):
        return User(first_name=self.first_name, last_name=self.last_name, email=self.email, password=self.password,
                    party=None, checkin_party=None)