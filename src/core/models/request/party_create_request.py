from dataclasses import dataclass
import datetime

from domain.entities.party import Party
from domain.enums.party_type import PartyType
from core.models.request.address_create_request import AddressCreateRequest
@dataclass
class PartyCreateRequest:
    name: str
    start_at: datetime
    end_at: datetime
    party_type: PartyType
    address: AddressCreateRequest

    

    def toEntity(self) -> Party:
        entity = Party(name=self.name, start_at=self.start_at, end_at=self.end_at, party_type=self.party_type, address=self.address.toEntity())
        return entity
