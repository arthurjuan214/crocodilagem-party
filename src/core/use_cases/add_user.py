from core.models.response.user_response import UserResponse
from domain.ports.repositories.user_repository import IUserRepository
from injector import inject
from core.models.request.user_register import UserRegister
from domain.entities.user import User
from infra.data.repositories.user_repository import UserRepository
from infra.token.token_service import TokenService
import datetime
from infra.data.models.user_model import UserModel
from infra.validator.email_validator import EmailValidator
from werkzeug.security import generate_password_hash
import re

class AddUser():
    @inject
    def __init__(self) -> None:
        self._userRepo = UserRepository()
        self._tokenServide = TokenService()
        self._emailValidator = EmailValidator()

    def add(self, userDto: UserRegister) -> UserResponse:

        isValidEmail = self._emailValidator.is_valid(userDto.email)
        print(isValidEmail)
        if isValidEmail is not True:
           raise Exception("Invalid Email")

        emailTaken = self._userRepo.get_user_by_email(userDto.email)
        if emailTaken is not None:
            raise Exception("Email already in use")       
       
        entity = userDto.to_entity()
        entity.password = generate_password_hash(userDto.password)
        model = UserModel(id=entity.id, first_name=entity.first_name, last_name=entity.last_name, email=entity.email,
                       password=entity.password, created_at=datetime.datetime.now(), updated_at=datetime.datetime.now() )

        
        userAdded = self._userRepo.add(model)
        
        #todo -> gerar token JWT
        token = self._tokenServide.generate_token(userAdded)

        result = UserResponse(
            id=model.id,           
            first_name=model.first_name,
            last_name=model.last_name,
            email=model.email,
            token=token
        )
        return result       
